import React, {useEffect, useState} from 'react';
import './App.css';

const App = () => {
  const ID = "f69a5f94";
  const KEY = "b8766f7f055383f29468910a6639fe9d	";

  useEffect( () => {
    
  });

  const getRecipes = async () => {
    const response = await fetch(`https://api.edamam.com/search?q=chicken&app_id=${ID}&app_key=${KEY}`);

  }
  return(
    <div className="App">
      <form className="search-form">
        <input className="search-bar" type="text"/>
        <button className="search-button" type="submit">Search</button>
      </form>
    </div>
  )
}

export default App;
